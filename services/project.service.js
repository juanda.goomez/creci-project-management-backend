const config = require('config.json');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const crypto = require("crypto");
const db = require('_helpers/db');
const Role = require('_helpers/role');
const mongoose = require('mongoose');
const Project = db.Project;


module.exports = {
    createProject,
    getAll,
    update,
    getAllProjectDeveloper

};

async function createProject(params, origin) {
    const project = new Project(params);
    await project.save();
}

async function getAll() {
    const projects = await Project.find().populate('developers');
    return projects;
}

async function getAllProjectDeveloper(idDeveloper) {
    const projects = await Project.find({ developers: mongoose.Types.ObjectId(idDeveloper) }).populate('developers', 'firstName');
    return projects;
}

async function update(id, params) {
    const project = await getProject(id);

    // copy params to project and save
    Object.assign(project, params);
    await project.save();

    return project;
}


async function getProject(id) {

    const project = await Project.findById(id);
    if (!project) throw 'Project not found';
    return project;
}