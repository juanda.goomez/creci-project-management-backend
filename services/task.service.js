const config = require('config.json');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const crypto = require("crypto");
const moment = require("moment");
const db = require('_helpers/db');
const Role = require('_helpers/role');
const Task = db.Task;
const Project = db.Project;
const User = db.User;

module.exports = {
    createTask,
    getAllTaskProject,
    addTaskDeveloper,
    getAllTaskDeveloper,
    getAllTaskProjectDeveloper,
    reportTime,
    getAllTask,
    updateTask,
    deleteReport

};

async function createTask(params, origin) {
    const task = new Task(params);
    await task.save();
}

async function updateTask(idTask, params) {
    const task = await getTask(idTask);
    var project = new Project();
    var developer = new User();
    if (params.project != undefined)
        project = await Project.findById(params.project);
    if (!project) throw 'project not found';
    if (params.developer != undefined)
        developer = await User.findById(params.developer);
    if (!developer) throw 'developer not found';
    Object.assign(task, params);
    await task.save();
    return task;
}

async function getAllTaskProject(projectId) {
    const tasks = await Task.find({ project: projectId });
    return tasks;
}

async function getAllTask() {
    const tasks = await Task.find().populate('developer', 'firstName');
    return tasks;
}

async function getAllTaskProjectDeveloper(projectId, developerId) {
    const tasks = await Task.find({ project: projectId, developer: developerId });
    return tasks;
}

async function getAllTaskDeveloper(developerId) {
    const tasks = await Task.find({ developer: developerId });
    return tasks;
}

async function addTaskDeveloper(idTask, idDeveloper) {
    const task = await getTask(idTask);

    task.developer = idDeveloper;
    await task.save();
    return task;

}

async function reportTime(idTask, time) {
    const task = await getTask(idTask);
    const today = moment();
    const aux = moment(task.timeReportDate);

    if (task.timeReportDate != undefined) {
        var duration = today.diff(aux, 'minutes');
        if (duration > 24) throw 'more than 24 hours have passed since your last report';
    }
    task.time = time;
    task.timeReportDate = new Date();
    await task.save();
    return task;

}

async function deleteReport(idTask) {
    const task = await getTask(idTask);
    const today = moment();
    const aux = moment(task.timeReportDate);
    if (task.timeReportDate != undefined) {
        var duration = today.diff(aux, 'minutes');
        if (duration > 24) throw 'more than 24 minutes have passed since your last report';
    }
    task.time = undefined;
    task.timeReportDate = undefined;
    await task.save();
    return task;

}


async function getTask(id) {
    if (!db.isValidId(id)) throw 'Task not found';
    const task = await Task.findById(id);
    if (!task) throw 'Task not found';
    return task;
}


