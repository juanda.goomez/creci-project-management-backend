const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const Project = new Schema({
    name: { type: String, required: true },
    description: { type: String },
    status: { type: String },
    time: { type: String },
    developers: [
        { type: mongoose.Schema.Types.ObjectId, ref: 'User' }
    ],
    created: { type: Date, default: Date.now }
});



Project.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: function (doc, ret) {
        // remove these props when object is serialized
        delete ret._id;
    }
});


module.exports = mongoose.model('Project', Project);