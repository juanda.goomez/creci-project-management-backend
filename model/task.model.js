const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const Task = new Schema({
  name: { type: String, required: true },
  description: { type: String },
  status: { type: String, required: true },
  time: { type: String },
  timeReportDate: { type: Date },
  developer: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  project: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Project'
  },
  created: { type: Date, default: Date.now },
});



Task.set('toJSON', {
  virtuals: true,
  versionKey: false,
  transform: function (doc, ret) {
    delete ret._id;
  }
});

module.exports = mongoose.model('Task', Task);