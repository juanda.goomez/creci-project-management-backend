require('rootpath')();
var morgan = require('morgan')
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const errorHandler = require('_middleware/error-handler');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(morgan('dev'));

// allow cors requests from any origin and with credentials
app.use(cors({ origin: (_origin, callback) => callback(null, true), credentials: true }));

// api routes
app.use('/tasks', require('./controller/task.controller'));
app.use('/users', require('./controller/user.controller'));
app.use('/projects', require('./controller/project.controller'));



// swagger docs route
app.use('/api-docs', require('_helpers/swagger'));

// global error handler
app.use(errorHandler);

// start server
const port = process.env.NODE_ENV === 'production' ? (process.env.PORT || 80) : 4000;
app.listen(port, () => {
    console.log('Server listening on port ' + port);
});
