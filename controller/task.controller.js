const express = require('express');
const router = express.Router();
const Joi = require('@hapi/joi');
const validateRequest = require('_middleware/validate-request');
const authorize = require('_middleware/authorize')
const Role = require('_helpers/role');
const taskService = require('../services/task.service');

router.post('/create-task', authorize(Role.Admin), validateCreateTask, createTask);
router.get('/', authorize(Role.Admin), getAllTask);
router.get('/:id', authorize(Role.Admin), getAllTaskProject);
router.get('/:idProject/:idDeveloper', authorize(), getAllTaskProjectDeveloper);

router.post('/:id', authorize(Role.User), getAllTaskDeveloper);
router.post('/:idTask/:time', authorize(Role.User), reportTime);

router.put('/:idTask/:idDeveloper', authorize(Role.Admin), addTaskDeveloper);
router.put('/:id', authorize(Role.Admin), updateTask);
router.delete('/:id', authorize(Role.User), deleteReport);




module.exports = router;

function validateCreateTask(req, res, next) {
    const schema = Joi.object({
        name: Joi.string().required(),
        status: Joi.string().required()
    });
    validateRequest(req, next, schema);
}

function createTask(req, res, next) {
    taskService.createTask(req.body, req.get('origin'))
        .then(() => res.json({ message: 'Registration successful' }))
        .catch(next);
}

function updateTask(req, res, next) {
    taskService.updateTask(req.params.id, req.body)
        .then(tasks => tasks ? res.json(tasks) : res.sendStatus(404))
        .catch(next);
}

function getAllTaskProject(req, res, next) {
    taskService.getAllTaskProject(req.params.id)
        .then(tasks => tasks ? res.json(tasks) : res.sendStatus(404))
        .catch(next);
}

function getAllTask(req, res, next) {
    taskService.getAllTask()
        .then(tasks => tasks ? res.json(tasks) : res.sendStatus(404))
        .catch(next);
}

function reportTime(req, res, next) {
    taskService.reportTime(req.params.idTask, req.params.time)
        .then(tasks => tasks ? res.json(tasks) : res.sendStatus(404))
        .catch(next);
}

function deleteReport(req, res, next) {
    taskService.deleteReport(req.params.id)
        .then(tasks => tasks ? res.json(tasks) : res.sendStatus(404))
        .catch(next);
}

function getAllTaskProjectDeveloper(req, res, next) {
    taskService.getAllTaskProjectDeveloper(req.params.idProject, req.params.idDeveloper)
        .then(tasks => tasks ? res.json(tasks) : res.sendStatus(404))
        .catch(next);
}

function getAllTaskDeveloper(req, res, next) {
    taskService.getAllTaskDeveloper(req.params.id)
        .then(tasks => tasks ? res.json(tasks) : res.sendStatus(404))
        .catch(next);
}

function addTaskDeveloper(req, res, next) {

    taskService.addTaskDeveloper(req.params.idTask, req.params.idDeveloper)
        .then(task => task ? res.json(task) : res.sendStatus(404))
        .catch(next);
}