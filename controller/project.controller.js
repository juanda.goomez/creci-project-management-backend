const express = require('express');
const router = express.Router();
const Joi = require('@hapi/joi');
const validateRequest = require('_middleware/validate-request');
const authorize = require('_middleware/authorize')
const Role = require('_helpers/role');
const projectService = require('../services/project.service');

router.post('/create-project', authorize(Role.Admin), validateCreateProject, createProject);
router.get('/', authorize(Role.Admin), getAll);
router.get('/:id', authorize(Role.User), getAllProjectDeveloper);
router.put('/:id', authorize(Role.Admin), update);




module.exports = router;

function validateCreateProject(req, res, next) {
    const schema = Joi.object({
        name: Joi.string().required(),
        status: Joi.string().required()
    });
    validateRequest(req, next, schema);
}

function createProject(req, res, next) {
    projectService.createProject(req.body, req.get('origin'))
        .then(() => res.json({ message: 'Registration successful' }))
        .catch(next);
}


function update(req, res, next) {
    projectService.update(req.params.id, req.body)
        .then(account => res.json(account))
        .catch(next);
}


function getAll(req, res, next) {
    projectService.getAll()
        .then(projects => res.json(projects))
        .catch(next);
}

function getAllProjectDeveloper(req, res, next) {
    projectService.getAllProjectDeveloper(req.params.id)
        .then(projects => res.json(projects))
        .catch(next);
}

